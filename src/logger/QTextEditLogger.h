#ifndef QTEXTEDITLOGGER_H
#define QTEXTEDITLOGGER_H

#include "ui_mainwindow.h"
#include <QMap>

class QTextEditLogger
{
public:
    QTextEditLogger(const QString &author, QTextEdit *editor = nullptr, qint32 level = 2);
    ~QTextEditLogger();

    void info(QString msg) const;
    void warn(QString msg) const;
    void debug(QString msg) const;
    void error(QString msg) const;

    void setLevel(const qint32 level);

public:
    enum Level {
        DEBUG,
        INFO,
        WARN,
        ERROR
    };

private:
    QTextEdit *_editor;
    Level _level;
    QString _author;
    QMap<Level, QString> _levelMap = {
        { Level::DEBUG, "DEBUG"},
        { Level::INFO, "INFO"},
        { Level::WARN, "WARN"},
        { Level::ERROR, "ERROR"},
    };

private:

    QString _buid(const QString &msg, Level level) const;

};

#endif // QTEXTEDITLOGGER_H
