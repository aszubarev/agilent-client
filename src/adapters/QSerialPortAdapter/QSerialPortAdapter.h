#ifndef COMPORTCONNECTION_H
#define COMPORTCONNECTION_H

#include <QSerialPortInfo>
#include <QString>
#include <QMap>
#include <QSerialPort>

#include <logger/QTextEditLogger.h>

class QSerialPortAdapter
{
public:
    QSerialPortAdapter(QObject *parent = nullptr, QTextEdit *loggerEditor = nullptr);
    ~QSerialPortAdapter();

    void open();

    void close();

    bool isOpen();

    QList<QSerialPortInfo> availablePorts();

    QString portName() const;
    void setPortName(const QString &portName);

    QString baudRateName() const;
    void setBaudRateName(const QString &baudRateName);

    QString dataBitsName() const;
    void setDataBitsName(const QString &dataBitsName);

    QString stopBitsName() const;
    void setStopBitsName(const QString &stopBitsName);

    QString parityName() const;
    void setParityName(const QString &parityName);

    QString flowControlName() const;
    void setFlowControlName(const QString &flowControlName);

    QString openModeFlagName() const;
    void setOpenModeFlagName(const QString &openModeFlagName);

    QSerialPort::BaudRate baudRate() const;
    void setBaudRate(const QSerialPort::BaudRate &baudRate);

    QSerialPort::DataBits dataBits() const;
    void setDataBits(const QSerialPort::DataBits &dataBits);

    QSerialPort::StopBits stopBits() const;
    void setStopBits(const QSerialPort::StopBits &stopBits);

    QSerialPort::Parity parity() const;
    void setParity(const QSerialPort::Parity &parity);

    QSerialPort::FlowControl flowControl() const;
    void setFlowControl(const QSerialPort::FlowControl &flowControl);

    QSerialPort::PinoutSignal pinoutSignal() const;
    void setPinoutSignal(const QSerialPort::PinoutSignal &pinoutSignal);

    QIODevice::OpenModeFlag openModeFlag() const;
    void setOpenModeFlag(const QIODevice::OpenModeFlag &openModeFlag);

    void write(const QByteArray &data, int writeTimout);
    QByteArray read(int readTimeout);

    QTextEditLogger *logger() const;

    bool isReadyRead();

private:

    QSerialPort *_serialPort;
    QTextEditLogger *_logger;

    QString _portName;
    QString _baudRateName;
    QString _dataBitsName;
    QString _stopBitsName;
    QString _parityName;
    QString _flowControlName;
    QString _openModeFlagName;

    QSerialPort::BaudRate _baudRate;
    QSerialPort::DataBits _dataBits;
    QSerialPort::StopBits _stopBits;
    QSerialPort::Parity _parity;
    QSerialPort::FlowControl _flowControl;
    QSerialPort::PinoutSignal _pinoutSignal;
    QIODevice::OpenModeFlag _openModeFlag;

    QMap<QString, QSerialPort::BaudRate> _baudRateMap = {

        { QString("1200  " ),   QSerialPort::Baud1200   },
        { QString("2400  " ),   QSerialPort::Baud2400   },
        { QString("4800  " ),   QSerialPort::Baud4800   },
        { QString("9600  " ),   QSerialPort::Baud9600   },
        { QString("19200 "),  QSerialPort::Baud19200  },
        { QString("38400 "),  QSerialPort::Baud38400  },
        { QString("57600 "),  QSerialPort::Baud57600  },
        { QString("115200"), QSerialPort::Baud115200 }
    };

    QMap<QString, QSerialPort::DataBits> _dataBitsMap = {
        { QString("5"), QSerialPort::Data5 },
        { QString("6"), QSerialPort::Data6 },
        { QString("7"), QSerialPort::Data7 },
        { QString("8"), QSerialPort::Data8 }
    };

    QMap<QString, QSerialPort::StopBits> _stopBitsMap = {
        { QString("OneStop"), QSerialPort::OneStop },
        { QString("OneAndHalfStop"), QSerialPort::OneAndHalfStop },
        { QString("TwoStop"), QSerialPort::TwoStop}
    };

    QMap<QString, QSerialPort::Parity> _parityMap = {
        { QString("NoParity"), QSerialPort::NoParity },
        { QString("EvenParity"), QSerialPort::EvenParity },
        { QString("OddParity"), QSerialPort::OddParity },
        { QString("SpaceParity"), QSerialPort::SpaceParity },
        { QString("MarkParity"), QSerialPort::MarkParity }
    };

    QMap<QString, QSerialPort::FlowControl> _flowControlMap = {
        { QString("NoFlowControl"), QSerialPort::NoFlowControl },
        { QString("HardwareControl"), QSerialPort::HardwareControl },
        { QString("SoftwareControl"), QSerialPort::SoftwareControl }
    };

    QMap<QString, QSerialPort::PinoutSignal> _pinoutSignalMap = {
        { QString("NoSignal"), QSerialPort::NoSignal },
        { QString("TransmittedDataSignal"), QSerialPort::TransmittedDataSignal },
        { QString("ReceivedDataSignal"), QSerialPort::ReceivedDataSignal },
        { QString("DataTerminalReadySignal"), QSerialPort::DataTerminalReadySignal },
        { QString("DataCarrierDetectSignal"), QSerialPort::DataCarrierDetectSignal },
        { QString("DataSetReadySignal"), QSerialPort::DataSetReadySignal },
        { QString("RingIndicatorSignal"), QSerialPort::RingIndicatorSignal },
        { QString("RequestToSendSignal"), QSerialPort::RequestToSendSignal },
        { QString("ClearToSendSignal"), QSerialPort::ClearToSendSignal },
        { QString("SecondaryTransmittedDataSignal"), QSerialPort::SecondaryTransmittedDataSignal },
        { QString("SecondaryReceivedDataSignal"), QSerialPort::SecondaryReceivedDataSignal }
    };

    QMap<QString, QIODevice::OpenModeFlag> _openModFlagMap = {
        { QString("NotOpen"),  QIODevice::NotOpen },
        { QString("ReadOnly"),  QIODevice::ReadOnly },
        { QString("WriteOnly"),  QIODevice::WriteOnly },
        { QString("ReadWrite"),  QIODevice::ReadWrite },
        { QString("Append"),  QIODevice::Append },
        { QString("Truncate"),  QIODevice::Truncate },
        { QString("Text"),  QIODevice::Text },
        { QString("Unbuffered"),  QIODevice::Unbuffered },
    };


private:

    QSerialPort::Parity _toParity(const QString &str) const;
    QSerialPort::BaudRate _toBaudRate(const QString &str) const;
    QSerialPort::DataBits _toDataBits(const QString &str) const;
    QSerialPort::StopBits _toStopBits(const QString &str) const;
    QSerialPort::FlowControl _toFlowControl(const QString &str) const ;
    QSerialPort::PinoutSignal _toPinoutSignal(const QString &str) const;
    QIODevice::OpenModeFlag _toOpenModeFlag(const QString &str) const;

    void _validate(QSerialPort::BaudRate baudRate, QSerialPort::Parity parity,
                   QSerialPort::StopBits stopBits, QSerialPort::DataBits dataBits,
                   QSerialPort::FlowControl flowControl, QIODevice::OpenModeFlag openModeFlag);
};

#endif // COMPORTCONNECTION_H
