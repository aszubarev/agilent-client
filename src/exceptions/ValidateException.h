#ifndef BAD_VALIDATE_EXCEPTION_H
#define BAD_VALIDATE_EXCEPTION_H

#include <stdexcept>

class ValidateException: public std::invalid_argument
{
public:
    ValidateException();
    ValidateException(const std::string &msg);
    ValidateException(const char *msg);
};

#endif // BAD_VALIDATE_EXCEPTION_H
