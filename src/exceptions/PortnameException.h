#ifndef EMPTY_PORTNAME_H
#define EMPTY_PORTNAME_H

#include <QException>

#include <stdexcept>


class PortNameException: public std::invalid_argument
{

public:

    PortNameException();
    PortNameException(const std::string &msg);
    PortNameException(const char *msg);

};

#endif // EMPTY_PORTNAME_H
