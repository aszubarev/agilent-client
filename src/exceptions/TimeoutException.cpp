#include "TimeoutException.h"

TimeoutException::TimeoutException():
    runtime_error("Excess imeout")
{}

TimeoutException::TimeoutException(const std::string &msg):
    runtime_error(msg)
{}

TimeoutException::TimeoutException(const char *msg):
    runtime_error(msg)
{}
