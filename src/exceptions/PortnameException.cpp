#include "PortnameException.h"

PortNameException::PortNameException():
    invalid_argument("bad port name")
{}

PortNameException::PortNameException(const std::string &msg):
    invalid_argument(msg)
{}

PortNameException::PortNameException(const char *msg):
    invalid_argument(msg)
{}

