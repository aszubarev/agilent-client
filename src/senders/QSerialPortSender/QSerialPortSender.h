#ifndef QSERIALPORTSENDER_H
#define QSERIALPORTSENDER_H

#include <QString>

#include <adapters/QSerialPortAdapter/QSerialPortAdapter.h>
#include <logger/QTextEditLogger.h>


class QSerialPortSender
{
public:

    QSerialPortSender(QSerialPortAdapter *qSerialPortAdapter = nullptr, QTextEdit *loggerEditor = nullptr);

    ~QSerialPortSender();

    QString writeAndRead(QString cmd);

    void write(QString cmd);

    QString read();

    void setPostfix(const QString &postfix);

    void setWriteTimeout(int writeTimeout);

    void setReadTimeout(int readTimeout);

    QTextEditLogger *logger() const;

    bool isReadyRead();

    bool isOpen();

private:

    QSerialPortAdapter *_qSerialPortAdapter;
    int _writeTimeout;
    int _readTimeout;
    QString _postfix;
    QTextEditLogger *_logger;

};

#endif // QSERIALPORTSENDER_H
