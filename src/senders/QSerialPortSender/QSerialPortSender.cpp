#include <QDebug>

#include "QSerialPortSender.h"
#include "exceptions/OpenPortException.h"
#include "exceptions/TimeoutException.h"

QSerialPortSender::QSerialPortSender(QSerialPortAdapter *qSerialPortAdapter, QTextEdit *loggerEditor):
    _qSerialPortAdapter(qSerialPortAdapter),
    _writeTimeout(200),
    _readTimeout(200),
    _postfix("\r\n")
{
    _logger = new QTextEditLogger("Sender", loggerEditor);
}

QSerialPortSender::~QSerialPortSender()
{
    delete _logger;
}

// clear direction
QString QSerialPortSender::writeAndRead(QString cmd)
{
    if (!_qSerialPortAdapter->isOpen()) { throw OpenPortException("Port is not open"); }
    cmd = cmd.trimmed();
    cmd += _postfix;

    try
    {
        _logger->debug("try send CMD: " + cmd);
        _qSerialPortAdapter->write(cmd.toUtf8(), _writeTimeout);
        QByteArray bResponse = _qSerialPortAdapter->read(_readTimeout);
        return QString(bResponse);
    }
    catch (TimeoutException &err)
    {
        QString msg = "Can't write and read cmd: " + cmd + "; cause: ";
        msg += err.what();
        throw TimeoutException(msg.toStdString());
    }
}

void QSerialPortSender::write(QString cmd)
{
    if (!_qSerialPortAdapter->isOpen()) { throw OpenPortException("Port is not open"); }
    cmd = cmd.trimmed();
    _logger->debug("try write cmd = " + cmd);
    cmd += _postfix;

    try
    {
        _qSerialPortAdapter->write(cmd.toUtf8(), _writeTimeout);
    }
    catch (TimeoutException &err)
    {
        QString msg = "Can't write cmd: " + cmd + "; cause: ";
        msg += err.what();
        throw TimeoutException(msg.toStdString());
    }
}

QString QSerialPortSender::read()
{
    if (!_qSerialPortAdapter->isOpen()) { throw OpenPortException("Port is not open"); }

    try
    {
//        _logger->debug("try read");
        QByteArray bResponse = _qSerialPortAdapter->read(_readTimeout);
        return QString(bResponse);
    }
    catch (TimeoutException &err)
    {
        throw TimeoutException(err.what());
    }
}

void QSerialPortSender::setPostfix(const QString &postfix)
{
     _logger->debug("set new postfix = " + postfix);
    _postfix = postfix;
}

void QSerialPortSender::setWriteTimeout(int writeTimeout)
{
    _logger->debug("set new writeTimeout = " + QVariant(writeTimeout).toString() + " [msec]");
    _writeTimeout = writeTimeout;
}

void QSerialPortSender::setReadTimeout(int readTimeout)
{
    _logger->debug("set new readTimeout = " + QVariant(readTimeout).toString() + " [msec]");
    _readTimeout = readTimeout;
}

QTextEditLogger *QSerialPortSender::logger() const
{
    return _logger;
}

bool QSerialPortSender::isReadyRead()
{
    return _qSerialPortAdapter->isReadyRead();
}

bool QSerialPortSender::isOpen()
{
    return _qSerialPortAdapter->isOpen();
}
