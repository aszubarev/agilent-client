#include "ui_mainwindow.h"
#include "main/mainwindow.h"
#include "logger/QTextEditLogger.h"


void MainWindow::_setupLogger()
{
    ui->logLevelComboBox->addItem("Error", QTextEditLogger::Level::ERROR);
    ui->logLevelComboBox->addItem("Debug", QTextEditLogger::Level::DEBUG);
    ui->logLevelComboBox->addItem("Info", QTextEditLogger::Level::INFO);
    ui->logLevelComboBox->addItem("Warn", QTextEditLogger::Level::WARN);
}
