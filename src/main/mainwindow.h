#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <listeners/QSerialPOrtListener.h>
#include <powerSource/PowerSource.h>
#include <senders/QSerialPortSender/QSerialPortSender.h>
#include "adapters/QSerialPortAdapter/QSerialPortAdapter.h"
#include "logger/QTextEditLogger.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void displayCurrent(QString value);
    void displayVoltage(QString value);
    void displayQSerialPortSenderAnswer(QString answer);

private slots:

    void on_adapterScanPushButton_clicked();

    void on_adapterClosePushButton_clicked();

    void on_adapterOpenPushButton_clicked();

    void on_postfixComboBox_currentTextChanged(const QString &arg1);

    void on_writeAndReadButton_clicked();

    void on_logLevelComboBox_currentIndexChanged(int index);

    void on_pushButton_2_clicked();

    void on_writeTimeOutSpinBox_valueChanged(int arg1);

    void on_spinBox_2_valueChanged(int arg1);

    void on_refreshButton_clicked();

    void on_resetButton_clicked();

    void on_refreshCurrentButton_clicked();

    void on_refreshVoltageButton_clicked();

    void on_writeButton_clicked();

    void on_sliderVolt_sliderMoved(int position);

    void on_sliderCurr_sliderMoved(int position);

    void on_decrVoltbutton_clicked();

    void on_incrVoltButton_clicked();

    void on_decrCurrButton_clicked();

    void on_incrCurrButton_clicked();

    void on_applyVoltbutton_clicked();

    void on_applyCurrbutton_clicked();

private:
    Ui::MainWindow *ui;
    QSerialPortAdapter *_qSerialPortAdapter;
    QSerialPortSender *_qSerialPortSender;
    QTextEditLogger *_logger;
    PowerSource *_powerSource;
    QSerialPortListener *_qSerialPortListener;

    int _applyingCurrent;
    int _applyingVoltage;

    int _maxCurrent;
    int _maxVoltage;

private:

    void _setupAdapterSettingsGroupBox();
    void _setupBaudRateComboBox();
    void _setupStopBitsComboBox();
    void _setupDataBitsComboBox();
    void _setupParityComboBox();
    void _setupFlowControlComboBox();
    void _setupOpenModFlagComboBox();
    void _setupPinoutSignalComboBox();

    void _setupLogger();

    void _setupSlotsConnections();

    int _getDigit(int value, int n);
};

#endif // MAINWINDOW_H
