#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QMessageBox>
#include <QMessageLogger>
#include <QThread>

#include "exceptions/PortnameException.h"
#include "exceptions/ValidateException.h"
#include "exceptions/OpenPortException.h"
#include "exceptions/TimeoutException.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    _logger = new QTextEditLogger("Main", ui->loggerTextEdit);
    _qSerialPortAdapter = new QSerialPortAdapter(this, ui->loggerTextEdit);
    _qSerialPortSender = new QSerialPortSender(_qSerialPortAdapter, ui->loggerTextEdit);
    _powerSource = new PowerSource(_qSerialPortSender, ui->loggerTextEdit);

    _setupLogger();
    _setupAdapterSettingsGroupBox();

    _qSerialPortListener = new QSerialPortListener(_qSerialPortSender);
    _qSerialPortListener->start();

    _setupSlotsConnections();

    _applyingCurrent = 0;
    _applyingVoltage = 0;
    _maxCurrent = 7000;
    _maxVoltage = 30000;

//    on_sliderCurr_sliderMoved(_maxCurrent);
//    on_sliderVolt_sliderMoved(0);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete _qSerialPortSender;
    delete _qSerialPortAdapter;
    delete _logger;
    delete _powerSource;

    _qSerialPortListener->terminate();
    delete _qSerialPortListener;
}

void MainWindow::on_postfixComboBox_currentTextChanged(const QString &arg1)
{
    _qSerialPortSender->setPostfix(arg1);
}

void MainWindow::on_writeAndReadButton_clicked()
{
    if (!_qSerialPortAdapter->isOpen()) {
        _qSerialPortAdapter->logger()->error("Port is not open");
        return;
    }

    try
    {
        QString cmd =  ui->commandEditor->toPlainText();
        _qSerialPortListener->setWaitAnswerType(QSerialPortListener::WaitAnswerType::QSerialPortSenderAnswer);
        _qSerialPortSender->write(cmd);

    } catch (TimeoutException &err) {
        ui->loggerTextEdit->append(err.what());
    } catch (OpenPortException &err) {
         ui->loggerTextEdit->append(err.what());
    }
}

void MainWindow::on_logLevelComboBox_currentIndexChanged(int index)
{
    QVariant data = ui->logLevelComboBox->itemData(index);
    _qSerialPortAdapter->logger()->setLevel(data.toInt());
    _qSerialPortSender->logger()->setLevel(data.toInt());
    _powerSource->logger()->setLevel(data.toInt());
    _logger->setLevel(data.toInt());
}

void MainWindow::on_pushButton_2_clicked()
{
    ui->loggerTextEdit->clear();
}

void MainWindow::on_writeTimeOutSpinBox_valueChanged(int arg1)
{
    _qSerialPortSender->setWriteTimeout(arg1);
}

void MainWindow::on_spinBox_2_valueChanged(int arg1)
{
    _qSerialPortSender->setReadTimeout(arg1);
}

void MainWindow::on_refreshButton_clicked()
{

    on_refreshCurrentButton_clicked();
    QThread::msleep(100);
    on_refreshVoltageButton_clicked();

}

void MainWindow::on_resetButton_clicked()
{
    _logger->debug("try reset");
    _powerSource->reset();
}

void MainWindow::on_refreshCurrentButton_clicked()
{
    _logger->debug("Try refresh Current");
    _qSerialPortListener->setWaitAnswerType(QSerialPortListener::WaitAnswerType::Current);
    _powerSource->getCurrent();
}

void MainWindow::on_refreshVoltageButton_clicked()
{

    _logger->debug("Try refresh Voltage");
    _qSerialPortListener->setWaitAnswerType(QSerialPortListener::WaitAnswerType::Voltage);
    _powerSource->getVoltage();
}


void MainWindow::on_writeButton_clicked()
{

    _logger->debug("on_writeButton_clickedr");
    if (!_qSerialPortAdapter->isOpen()) {
        _qSerialPortAdapter->logger()->error("Port is not open");
        return;
    }

    try {
        QString cmd =  ui->commandEditor->toPlainText();
        _qSerialPortSender->write(cmd);
    } catch (TimeoutException &err) {
        ui->loggerTextEdit->append(err.what());
    } catch (OpenPortException &err) {
         ui->loggerTextEdit->append(err.what());
    } catch (std::exception &err) {
        _logger->error(err.what());
    }
}

void MainWindow::on_sliderVolt_sliderMoved(int position)
{
    _applyingVoltage = position;
    ui->lcdVolt1->display(_getDigit(position, 1));
    ui->lcdVolt2->display(_getDigit(position, 2));
    ui->lcdVolt3->display(_getDigit(position, 3));
    ui->lcdVolt4->display(_getDigit(position, 4));

    if (_applyingVoltage > 15000 && _maxCurrent != 4000) {
        _logger->debug("_applyingCurrent > 15000 && _maxCurrent != 4000");
        _maxCurrent = 4000;
        if (_applyingCurrent > _maxCurrent) {
            _applyingCurrent = _maxCurrent;
            ui->sliderCurr->setValue(_applyingCurrent);
            on_sliderCurr_sliderMoved(_applyingCurrent);
//            ui->currentLCD->display(_maxCurrent / 1000);
        }
        ui->sliderCurr->setRange(0, _maxCurrent);
    } else if (_applyingVoltage <= 15000 && _maxCurrent != 7000) {
        _logger->debug("_applyingVoltage <= 15000 && _maxCurrent != 7000");
        _maxCurrent = 7000;
        ui->sliderCurr->setRange(0, _maxCurrent);
    }
}

void MainWindow::on_sliderCurr_sliderMoved(int position)
{
    _applyingCurrent = position;
    ui->lcdCurr1->display(_getDigit(position, 1));
    ui->lcdCurr2->display(_getDigit(position, 2));
    ui->lcdCurr3->display(_getDigit(position, 3));
    ui->lcdCurr4->display(_getDigit(position, 4));
}

int MainWindow::_getDigit(int value, int n)
{
    if (n > 4) {
        _logger->error("[_getDigit] n > 3");
        return 0;
    }

    if (n == 1) {

        if (value < 1000) { return 0; }
        return value / 1000;

    } else if (n == 2) {

        if (value < 100) { return 0; }
        int tmp = value / 100;
        return tmp % 10;

    } else if (n == 3) {

        if (value < 10) { return 0; }
        int tmp = value / 10;
        return tmp % 10;

    } else if (n == 4) {
        return value % 10;
    }

    return 0;
}

void MainWindow::on_decrVoltbutton_clicked()
{
    if (_applyingVoltage == 0) { return; }

    _applyingVoltage -= 1;
    ui->sliderVolt->setValue(_applyingVoltage);
    on_sliderVolt_sliderMoved(_applyingVoltage);
}

void MainWindow::on_incrVoltButton_clicked()
{
    if (_applyingVoltage == _maxVoltage) { return; }

    _applyingVoltage += 1;
    ui->sliderVolt->setValue(_applyingVoltage);
    on_sliderVolt_sliderMoved(_applyingVoltage);
}

void MainWindow::on_decrCurrButton_clicked()
{
    if (_applyingCurrent == 0) { return; }

    _applyingCurrent -= 1;
    ui->sliderCurr->setValue(_applyingCurrent);
    on_sliderCurr_sliderMoved(_applyingCurrent);
}

void MainWindow::on_incrCurrButton_clicked()
{
    if (_applyingCurrent == _maxCurrent) { return; }

    _applyingCurrent += 1;
    ui->sliderCurr->setValue(_applyingCurrent);
    on_sliderCurr_sliderMoved(_applyingCurrent);
}

void MainWindow::on_applyVoltbutton_clicked()
{
    double voltage = QVariant(_applyingVoltage).toDouble() / 1000.0;
    _logger->debug("try applay voltage = " + QVariant(voltage).toString());
    bool ok = _powerSource->applyVoltage(voltage);
    if (ok == true) {
        _logger->debug("apply voltage ok");
        ui->voltageLCD->display(voltage);
    } else {
        _logger->error("BAD APPLY VOLTAGE");
    }
}

void MainWindow::on_applyCurrbutton_clicked()
{
    double current = QVariant(_applyingCurrent).toDouble() / 1000.0;
    _logger->debug("try applay current = " + QVariant(current).toString());
    bool ok = _powerSource->applyCurrent(current);
    if (ok == true) {
        _logger->debug("apply current ok");
        ui->currentLCD->display(current);
    } else {
        _logger->error("BAD APPLY CURRENT");
    }
}
