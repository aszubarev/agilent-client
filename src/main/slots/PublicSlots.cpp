#include "ui_mainwindow.h"
#include "main/mainwindow.h"


void MainWindow::displayCurrent(QString value)
{
    _logger->debug("arrived current = " + value);
    ui->currentLCD->display(value.toDouble());
}

void MainWindow::displayVoltage(QString value)
{
    _logger->debug("arrived voltage = " + value);
    ui->voltageLCD->display(value.toDouble());
}

void MainWindow::displayQSerialPortSenderAnswer(QString answer)
{
    _logger->debug("arrived answer for QSerialPortSender = " + answer);
    ui->answer->setPlainText(answer);
}
