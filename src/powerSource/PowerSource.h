#ifndef POWERSOURCE_H
#define POWERSOURCE_H

#include <QTextEdit>

#include <senders/QSerialPortSender/QSerialPortSender.h>



class PowerSource
{
public:
    PowerSource(QSerialPortSender *sender = nullptr, QTextEdit *loggerEditor = nullptr);
    ~PowerSource();

    void getCurrent();
    void getVoltage();
    void reset();

    bool applyVoltage(double value);
    bool applyCurrent(double value);

    QTextEditLogger *logger() const;

private:

    const QString _getCurrentCMD;
    const QString _getVoltageCMD;
    const QString _resetCMD;
    const QString _applyVoltCMDTemplate;
    const QString _applyCurrCMDTemplate;

    const double _defaultCurrentReturning;
    const double _defaultVoltageReturning;

    QSerialPortSender *_sender;
    QTextEditLogger *_logger;
};

#endif // POWERSOURCE_H
