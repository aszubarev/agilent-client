#include "PowerSource.h"

#include <logger/QTextEditLogger.h>

#include <exceptions/OpenPortException.h>
#include <exceptions/TimeoutException.h>

PowerSource::PowerSource(QSerialPortSender *sender, QTextEdit *loggerEditor):
    _getCurrentCMD("SOURce:CURRent?"),
    _getVoltageCMD("SOURce:VOLTage?"),
    _applyCurrCMDTemplate("CURR "),
    _applyVoltCMDTemplate("VOLT "),
    _defaultCurrentReturning(0.0),
    _defaultVoltageReturning(0.0)
{
    _sender = sender;
    _logger = new QTextEditLogger("PowerSource", loggerEditor);
}

PowerSource::~PowerSource()
{
    delete _logger;
}

void PowerSource::getCurrent()
{
    if (_sender == nullptr)
    {
        _logger->error("Sender is not initialized");
        return;
    }

    try
    {
        _logger->debug("try get current");
        _sender->write(_getCurrentCMD);
    }
    catch (TimeoutException &err)
    {
        _logger->error(err.what());
    }
    catch (OpenPortException &err)
    {
        _logger->error(err.what());
    }
}

void PowerSource::getVoltage()
{
    if (_sender == nullptr) {
        _logger->error("Sender is not initialized");
        return;
    }

    try {
       _logger->debug("try get voltage");
       _sender->write(_getVoltageCMD);
    } catch (TimeoutException &err) {
        _logger->error(err.what());
    } catch (OpenPortException &err) {
        _logger->error(err.what());
    }
}

void PowerSource::reset()
{
    if (_sender == nullptr)
    {
        _logger->error("Sender is not initialized");
        return;
    }

    try
    {
        _logger->debug("try reset");
        _sender->write(_resetCMD);
    }
    catch (TimeoutException &err)
    {
        _logger->error(err.what());
        return;
    }
    catch (OpenPortException &err)
    {
        _logger->error(err.what());
        return;
    }
}

bool PowerSource::applyVoltage(double value)
{
    if (_sender == nullptr)
    {
        _logger->error("Sender is not initialized");
        return false;
    }

    try
    {
        QString voltage = QVariant(value).toString();
        QString cmd = _applyVoltCMDTemplate + voltage;
        _logger->debug("try apply voltage = " + voltage);
        _sender->write(cmd);
        return true;
    }
    catch (TimeoutException &err)
    {
        _logger->error(err.what());
        return false;
    }
    catch (OpenPortException &err)
    {
        _logger->error(err.what());
        return false;
    }
}

bool PowerSource::applyCurrent(double value)
{
    if (_sender == nullptr)
    {
        _logger->error("Sender is not initialized");
        return false;
    }

    try
    {
        QString current = QVariant(value).toString();
        QString cmd = _applyCurrCMDTemplate + current;
        _logger->debug("try apply current = " + current);
        _sender->write(cmd);
        return true;
    }
    catch (TimeoutException &err)
    {
        _logger->error(err.what());
        return false;
    }
    catch (OpenPortException &err)
    {
        _logger->error(err.what());
        return false;
    }
}

QTextEditLogger *PowerSource::logger() const
{
    return _logger;
}
