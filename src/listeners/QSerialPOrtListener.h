#ifndef QSERIALPORTLISTENER_H
#define QSERIALPORTLISTENER_H

#include <QThread>

#include <adapters/QSerialPortAdapter/QSerialPortAdapter.h>

#include <senders/QSerialPortSender/QSerialPortSender.h>

class QSerialPortListener : public QThread
{

Q_OBJECT
    void run();

signals:
    void currentReady(QString s);
    void voltageReady(QString s);
    void idnReady(QString s);
    void qSerialPortSenderReady(QString s);

public:
    QSerialPortListener(QSerialPortSender *sender = nullptr);

    void setWaitAnswerType(quint32 type);

    enum WaitAnswerType {
        None,
        Current,
        Voltage,
        IDN,
        QSerialPortSenderAnswer
    };

    bool isRunning() const;
    void lock();
    void unlock();

private:
    QSerialPortSender *_sender;
    WaitAnswerType _waitAnswerType;

    bool _isRunning;
};

#endif // QSERIALPORTLISTENER_H
